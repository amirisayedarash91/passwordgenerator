Password Generator je aplik�cia s otvoren�m zdrojov�m k�dom, ktor� generuje bezpe�n� hesl� pomocou kryptograficky bezpe�n�ho gener�tora pseudo-n�hodn�ch ��siel. Jednoducho si vyberte ktor� znaky m� heslo obsahova� z ponuky alebo si m��ete zvoli� vlastn� znaky a kliknite na tla�idlo generova�.


Funkcie:
� Ve�mi intuit�vny dizajn, sta�� iba klikn�� na tla�idlo
� Jednoducho vyberte ktor� znaky m� heslo obsahova�
� Hesl� s� generovan� pomocou kryptograficky bezpe�n�ho gener�tora pseudo-n�hodn�ch ��siel
� Nie s� vy�adovan� �iadne povolenia ani na internet ani �lo�isko, hesl� nie s� nikdy nikde ukladan�
� Heslo m��e ma� 1 - 999 znakov
� Mo�nos� generova� a� 99 hesiel s��asne
� Zvo�te si vlastn� znaky, ktor� m� heslo obsahova�
� Pou�ite vlastn� seed na generovanie hesiel
� Zobrazuje silu hesla a po�et bitov entropie
� Automaticky vymaz�va obsah schr�nky
� D� sa pou�i� ako gener�tor n�hodn�ch ��siel
� Nevy�aduje �iadne povolenia
� Svetl� a tmav� vzh�ad
� Aplik�cia s otvoren�m zdrojov�m k�dom
� Neobsahuje reklamy
